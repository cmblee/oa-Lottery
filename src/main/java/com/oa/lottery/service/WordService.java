package com.oa.lottery.service;

import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.util.IOUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

@Service
public class WordService {
    public void createWord() {
        String html = "<div class=\"print-cont\">\n" +
                "                 <input type=\"hidden\" name=\"hfPwd\" id=\"hfPwd\">\n" +
                "        <div style=\"vertical-align: top\">\n" +
                "            <table id=\"tbQuestion\" width=\"660\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\" style=\"vertical-align: top;font-size:14px;\">\n" +
                "\t<tbody><tr>\n" +
                "\t\t<td>\n" +
                "                        <table width=\"660\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n" +
                "                         \n" +
                "                            <tbody><tr>\n" +
                "                                <td width=\"660\" height=\"30\" colspan=\"2\" align=\"center\">\n" +
                "                                    <div id=\"div_title\" style=\"font-size:16px; font-weight:bold;\">test</div>\n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                            <tr>\n" +
                "                                <td>\n" +
                "                                    <div id=\"div_discription\" style=\"margin: 5px;padding: 5px;\"></div>                 \n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                        </tbody></table>\n" +
                "                        <div id=\"question\"><table width=\"660\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#FFFFFF\"><tbody><tr><td><br></td></tr><tr><td align=\"left\"><b>1.</b>  标题[单选题] <span style=\"color:#f00\"> *</span></td></tr><tr><td align=\"left\">&nbsp;&nbsp;&nbsp;<span style=\"font-size:24px;\">○</span>&nbsp;选项4<br>&nbsp;&nbsp;&nbsp;<span style=\"font-size:24px;\">○</span>&nbsp;选项5</td></tr><tr style=\"height:30px;\"><td></td></tr><tr><td align=\"left\"><b>2.</b>  标题  [多选题]  <span style=\"color:#f00\"> *</span></td></tr><tr><td align=\"left\">&nbsp;&nbsp;&nbsp;<span style=\"font-size:24px;\">□</span>&nbsp;选项2<br>&nbsp;&nbsp;&nbsp;<span style=\"font-size:24px;\">□</span>&nbsp;选项3</td></tr><tr style=\"height:30px;\"><td></td></tr><tr><td align=\"left\"><b>3.</b>  标题  [填空题]  <span style=\"color:#f00\"> *</span></td></tr><tr><td height=\"20px\"></td></tr><tr><td align=\"left\">&nbsp;&nbsp;_________________________________</td></tr><tr style=\"height:30px;\"><td></td></tr></tbody></table></div>\n" +
                "                               \n" +
                "                      \n" +
                "                    </td>\n" +
                "\t</tr>\n" +
                "</tbody></table>\n" +
                "\n" +
                "        </div>\n" +
                "            </div>";

        POIFSFileSystem poifs = null;
        FileOutputStream ostream = null;
        ByteArrayInputStream bais = null;
        String uuid = "测试.doc";
        File file = null;

        try {
            //HTML内容必须被<html><body></body></html>包装
            html = ("<html><body>" + html + "</body></html>");
            byte[] b = html.getBytes(StandardCharsets.UTF_8);
            bais = new ByteArrayInputStream(b);
            poifs = new POIFSFileSystem();
            DirectoryEntry directory = poifs.getRoot();
            //WordDocument名称不允许修改
            directory.createDocument("WordDocument", bais);
            ostream = new FileOutputStream(uuid);
            poifs.writeFilesystem(ostream);//当前目录下就生成了一个测试.doc的文档
        } catch (Exception e) {
            //　　logger.error("exception is {}", e);
        } finally {
            IOUtils.closeQuietly(poifs);
            IOUtils.closeQuietly(ostream);
            IOUtils.closeQuietly(bais);
            try {
                FileUtils.forceDelete(file);
            } catch (Exception e2) {
            }
        }
    }
}
